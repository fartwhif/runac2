﻿using Microsoft.Win32;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Data;

namespace RunAC2
{
    public static class Constants
    {
        public const string DefaultServer = "127.0.0.1:7777";
        public const string DefaultAC2ClientPath = "C:\\Games\\Turbine\\Asheron's Call 2\\AC2client.exe";
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        public Config config = new Config();
        public MainWindow()
        {
            InitializeComponent();
            config.Load();
            DataContext = config;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!config.Server.Contains(":"))
            {
                MessageBox.Show($"Please check the value for server, it should like something like {Constants.DefaultServer}");
                return;
            }
            if (!File.Exists(config.AcclientPath))
            {
                MessageBox.Show($"File does not exist: {config.AcclientPath}");
                return;
            }
            Thread thread = new Thread(() =>
            {
                try
                {
                    var srvSplit = config.Server.Split(':');

                    string arguments = $"--host {srvSplit[0]} --port {srvSplit[1]} --rodat on --account {config.User} --glsticketdirect {config.Pass}";
                    if (StartProc(config.AcclientPath, arguments, config.DPIFix) == -1)
                    {
                        MessageBox.Show("Unable to launch AC2 launcher", "error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            });
            thread.Start();
        }
        private static int StartProc(string exeFilPath, string arguments, bool DPIFix)
        {
            if (DPIFix)
            {
                try
                {
                    //HKEY_CURRENT_USER\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers

                    var compatSettings = "~ GDIDPISCALING DPIUNAWARE";

                    RegistryKey key = Registry.CurrentUser.OpenSubKey("Software", false);

                    key = key.OpenSubKey("Microsoft", false);
                    key = key.OpenSubKey("Windows NT", false);
                    key = key.OpenSubKey("CurrentVersion", false);
                    key = key.OpenSubKey("AppCompatFlags", false);
                    key = key.OpenSubKey("Layers", true);

                    bool ok = false;
                    object? r = key.GetValue(exeFilPath);
                    if (r != null)
                    {
                        var g = (string)r;
                        if (g == compatSettings)
                        {
                            ok = true;
                        }
                    }
                    if (!ok)
                    {
                        key.SetValue(exeFilPath, compatSettings);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Failed to apply DPI fix because: {Environment.NewLine}{Environment.NewLine}{ex.ToString()}");
                    return -1;
                }
            }

            try
            {
                ProcessStartInfo psi = new ProcessStartInfo
                {
                    FileName = exeFilPath,
                    Arguments = arguments,
                    WorkingDirectory = Path.GetDirectoryName(exeFilPath)
                };
                Process newProc = Process.Start(psi);
                return newProc.Id;
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to run AC2");
            }
            return -1;
        }
    }



    public class Config : INotifyPropertyChanged
    {
        public Config() { }
        private bool Loading = false;
        public void Load()
        {
            try
            {
                Loading = true;
                string cfgLoc = cfg_location();
                if (!File.Exists(cfgLoc))
                {
                    var fs = File.Create(cfgLoc);
                    fs.Close();
                    if (File.Exists(Constants.DefaultAC2ClientPath))
                    {
                        AcclientPath = Constants.DefaultAC2ClientPath;
                    }
                    if (string.IsNullOrWhiteSpace(AcclientPath))
                    {
                        var lookForAc2 = Path.Combine(Path.GetDirectoryName(cfgLoc), "AC2client.exe");
                        if (File.Exists(lookForAc2))
                        {
                            AcclientPath = lookForAc2;
                        }
                    }
                    Server = Constants.DefaultServer;
                    Save();
                }
                else
                {
                    string[] arLines = File.ReadAllLines(cfgLoc);
                    if (arLines.Length > 4)
                    {
                        AcclientPath = arLines[0];
                        Server = arLines[1];
                        User = arLines[2];
                        Pass = arLines[3];
                        DPIFix = bool.Parse(arLines[4]);
                    }
                }
            }
            catch (Exception ex) { }
            Loading = false;
        }
        private string cfg_location()
        {
            string r = string.Empty;
            var eaLoc = Assembly.GetExecutingAssembly().Location;
            var cfgLoc2 = Path.Combine(Environment.CurrentDirectory, Path.GetFileName(eaLoc) + ".cfg");
            var cfgLoc = Path.Combine(Path.GetDirectoryName(eaLoc), Path.GetFileName(eaLoc) + ".cfg");

            if (File.Exists(cfgLoc2))
            {
                r = cfgLoc2;
            }
            else if (File.Exists(cfgLoc))
            {
                r = cfgLoc;
            }
            else
            {
                r = cfgLoc2;
            }
            return r;
        }
        public void Save()
        {
            try
            {
                string cfgLoc = cfg_location();
                List<string> data = new List<string>
                {
                    AcclientPath,
                    Server,
                    User,
                    Pass,
                    DPIFix.ToString(),
                };
                File.WriteAllLines(cfgLoc, data);
            }
            catch (Exception ex) { }
        }

        private string _AcclientPath { get; set; }
        private string _Server { get; set; }
        private string _User { get; set; }
        private string _Pass { get; set; }
        private bool _DPIFix { get; set; }

        public string AcclientPath
        {
            get { return _AcclientPath; }
            set
            {
                _AcclientPath = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("AcclientPath"));
                }
                if (!Loading)
                {
                    Save();
                }
            }
        }
        public string Server
        {
            get { return _Server; }
            set
            {
                _Server = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Server"));
                }
                if (!Loading)
                {
                    Save();
                }
            }
        }
        public string User
        {
            get { return _User; }
            set
            {
                _User = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("User"));
                }
                if (!Loading)
                {
                    Save();
                }
            }
        }
        public string Pass
        {
            get { return _Pass; }
            set
            {
                _Pass = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Pass"));
                }
                if (!Loading)
                {
                    Save();
                }
            }
        }
        public bool DPIFix
        {
            get { return _DPIFix; }
            set
            {
                _DPIFix = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("DPIFix"));
                }
                if (!Loading)
                {
                    Save();
                }
            }
        }

        public event PropertyChangedEventHandler? PropertyChanged;
    }
    public class RadioBoolToIntConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int integer = (int)value;
            if (integer == int.Parse(parameter.ToString()))
                return true;
            else
                return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return parameter;
        }
    }
}
